<?php
/**
 * Sets our SCSS constant for the improved build process
 * REQUIRED
 *
 * @since 2.7.0
 */
if ( !defined( 'HJI_BLVD_SCSS' ) ) :
    define( 'HJI_BLVD_SCSS', true );
endif;

/**
 * These next functions are scaffolding for some often-needed customizations.
 * Uncomment the add_action lines to activate the functions.
 */

/**
 * Remove parent theme widget areas & registering a sample child widget area. 
 */
if ( !function_exists( 'hji_child_widgets_init' ) ) :
    function hji_child_widgets_init() {
        unregister_sidebar( 'idx-horizontal-search' );
        unregister_sidebar( 'blvd-main-sidebarwidgets' );
        unregister_sidebar( 'blvd-topbar-sidebarwidgets' );
        unregister_sidebar( 'blvd-footerwidgets' );
        unregister_sidebar( 'blvd-header-sidebarwidgets' );

        register_sidebar( array(
            'id' => 'blvd-example-widget',
            'name' => __( 'Example Widget', 'hji_themework'),
            'description' => __('This is an example',
            'before-widget' => '<div="%1$s" class="widget-container %2$s"><div class="widget-innner">',
            'after-widget' => '</div></div>',
            'before-title' => '<div class="widget-title"><h3>',
            'after-title' => '</div></div>',
        ));
    }
    // add_action( 'widgets_init', 'hji_child_widgets_init', 11 );
endif;

/**
 * loading in child theme js
 */
if ( !function_exists( 'hji_child_enqueque_scripts' ) ) :
    function hji_child_enqueque_scripts() {
        wp_enqueue_script( 'hji_child_scripts', get_stylesheet_directory_uri() . '/assets/js/main.js' );
    }
    // add_action( 'wp_enqueue_scripts', 'hji_child_enqueque_scripts' );
endif;

/**
 * adding the menu bar under the header if there is a menu in the "Main Menu" role.
 */
if (!function_exists('hji_child_menu_init')) {
    function hji_child_menu_init() {
        if( has_nav_menu('main-menu')) {
            $args = array(
                'menu' => 'main-menu',
                );
            $atts = array(
                'before_widget' => '<div class="col-xs-12 container-fluid header-main-menu">',
                'after_widget' => '</div>',
                );
            the_widget('HJI_MainMenuWidget', $args, $atts);
        }
    }
    // add_action('hji_theme_after_navbar_nav', 'hji_child_menu_init', 11);
}

/**
 * Making the social icons accessible for use in any area.
 * This one isn't hooked to anything, just call the function directly in your templates.
 */
if (!function_exists('hji_child_social_icons')) {
    function hji_child_social_icons() {
        global $hji_theme_options;

        $links = hji_social_media_links();

        $output = '<div class="socialmedia-icons">' . $links . '</div>';

        echo $output;
    }
}
